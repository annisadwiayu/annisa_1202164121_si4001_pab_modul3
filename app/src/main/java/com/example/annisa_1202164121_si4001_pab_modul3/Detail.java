package com.example.annisa_1202164121_si4001_pab_modul3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Detail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ImageView foto = findViewById(R.id.foto);
        TextView tvNama = findViewById(R.id.txt_nama);
        TextView tvPekerjaan = findViewById(R.id.txt_job);

        Intent dataIntent = getIntent();
        User u = dataIntent.getParcelableExtra("data");

        if (u.getFoto() == 0) {
            foto.setImageDrawable(getResources().getDrawable(R.drawable.laki));
        } else {
            foto.setImageDrawable(getResources().getDrawable(R.drawable.cewe));
        }

        tvNama.setText(u.getNama());
        tvPekerjaan.setText(u.getJob());
    }
}
