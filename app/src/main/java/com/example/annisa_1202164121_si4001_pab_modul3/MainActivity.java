package com.example.annisa_1202164121_si4001_pab_modul3;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<User> listUser = new ArrayList<User>();
    private Adapter adapterUser;
    private String[] jk = new String[]{"Male", "Female"};
    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.adapterUser = new Adapter(this, listUser);
        rv = findViewById(R.id.recyclerview);
        rv.setAdapter(adapterUser);
        rv.setLayoutManager(new LinearLayoutManager(this));


        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP,
                ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int from = viewHolder.getAdapterPosition();
                int to = target.getAdapterPosition();
                Collections.swap(listUser, from ,to);
                adapterUser.notifyItemMoved(from,to);

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                listUser.remove(viewHolder.getAdapterPosition());
                adapterUser.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        helper.attachToRecyclerView(rv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public void fabListener(View view) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        final View dialog = this.getLayoutInflater().inflate(R.layout.design_dialog, null);
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, jk);
        Spinner s = dialog.findViewById(R.id.spinner);
        s.setAdapter(adapter);

        final EditText etNama = dialog.findViewById(R.id.nama_text);
        final EditText etPekerjaan = dialog.findViewById(R.id.editText_pekerjaan);
        Button btnSubmit = dialog.findViewById(R.id.submit);
        Button btnCancel = dialog.findViewById(R.id.cancel);
        builder.setView(dialog);
        final AlertDialog ad = builder.show();

        final User u = new User();

        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                u.setFoto(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = etNama.getText().toString();
                String pekerjaan = etPekerjaan.getText().toString();

                u.setNama(nama);
                u.setJob(pekerjaan);

                listUser.add(u);
                ad.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){
            rv.setLayoutManager(new GridLayoutManager(this, 2));
        }else if (newConfig.orientation==Configuration.ORIENTATION_PORTRAIT){
            rv.setLayoutManager(new LinearLayoutManager(this));
        }
    }

}
