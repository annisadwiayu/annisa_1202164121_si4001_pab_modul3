package com.example.annisa_1202164121_si4001_pab_modul3;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    public String nama;
    public String job;
    public int foto;

    public User() {
    }

    public User(String nama, String job, int foto) {
        this.nama = nama;
        this.job = job;
        this.foto = foto;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nama);
        dest.writeString(this.job);
        dest.writeInt(this.foto);
    }

    protected User(Parcel in) {
        this.nama = in.readString();
        this.job = in.readString();
        this.foto = in.readInt();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
